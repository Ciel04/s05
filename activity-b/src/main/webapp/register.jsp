<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Job Finder</title>
</head>
<body>
	<h1>Registration confirmation</h1>
	<% 
		String discover = session.getAttribute("discover").toString();
		
		if(discover.equals("friends")){
			discover="Friends";
		}else if(discover.equals("socialmedia")){
			discover="Social Media";
		}else
			discover="Others"; 
		
	
		String dateOfBirth = session.getAttribute("dateOfBirth").toString();
		
	%> 
	
	<p>First Name: <%= session.getAttribute("firstName") %></p>
	<p>Last Name: <%= session.getAttribute("lastName") %></p>
	<p>Phone: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= discover %></p>
	<p>Date of Birth: <%= dateOfBirth %></p>
	<p>User Type: <%= session.getAttribute("userType") %> </p>
	<p>Description: <%= session.getAttribute("description") %></p>
	
	<form action="login" method="post">
		<input type="submit">
	</form>
	
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>
</body>
</html>