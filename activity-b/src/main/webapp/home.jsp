<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Job Finder</title>
</head>
<body>
	<% 
		String fullName = session.getAttribute("fullName").toString();
	%>
	<h1>Welcome <%= fullName %>!</h1>
	<%
		String message="";
	
		String userType = session.getAttribute("userType").toString();
		
		if(userType.equals("Applicant")){
			message="Welcome applicant. You may now start looking for your career oppurtunity.";
		}
		if(userType.equals("Employer")){
			message="Welcome employer. You may now start browsing applicant profiles.";
		}
	%>
	
	<p><%= message %></p>
</body>
</html>