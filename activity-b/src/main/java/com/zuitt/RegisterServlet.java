package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	public void init() throws ServletException{
		System.out.println("Booking Servlet has been initialized");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String firstName = req.getParameter("firstname");
		String lastName = req.getParameter("lastname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discover = req.getParameter("discover");
		String dateOfBirth = req.getParameter("date_of_birth");
		String userType = req.getParameter("user_type");
		String description = req.getParameter("description");
		
		//Stores all the data from the form into the session
		HttpSession session = req.getSession();
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discover", discover);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("userType", userType);
		session.setAttribute("description", description);
				
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("BookingServlet has been destryoed");
	}
}
